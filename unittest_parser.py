#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase as TC
from unittest import main
from parser import LogEntry, parse_alog_line

class ParserTC(TC):

    def setUp(self):
        # s = '''18.1.1.201 - - [08/Sep/2013:06:29:02 +0200]
        # "GET / HTTP/1.1" 302 312 "-" "check_http/v1.4.15
        # (nagios-plugins 1.4.15)" 530'''
        logline = """127.0.0.1 user-identifier frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 "-" "check_http/v1.4.15 (nagios-plugins 1.4.15)" 530"""
        self.parser = parse_alog_line(logline)

    def test_one_line(self):
        self.assertEqual(self.parser.ip, "127.0.0.1")
        self.assertEqual(self.parser.user , "frank")
        self.assertEqual(self.parser.verb , "GET")
        self.assertEqual(self.parser.resp_code, "200")
        self.assertEqual(self.parser.resp_length, "2326")
        self.assertEqual(self.parser.referrer, '"-"')
        self.assertEqual(self.parser.req_length, "530")

    def test_match(self):
        self.assertTrue(self.parser.match(ip='127.0.0.1'))
        self.assertFalse(self.parser.match(ip='128.0.0.1'))
        self.assertFalse(self.parser.match(toto='127.0.0.1'))

    def test_file(self):
        with open('access.log') as f:
            for line in f.xreadlines():
                parse_alog_line(line)

if __name__ == "__main__":
    main()
