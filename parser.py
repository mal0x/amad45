#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

class LogEntry(object):

    def __init__(self, **kwargs):
        for k,v in kwargs.iteritems():
            setattr(self, k, v)
        # assert self.verb in ('GET', 'POST'), "unkown verb"

    def match(self, **kwargs):
        for k,v in kwargs.iteritems():
            if not hasattr(self, k):
                return False
            if getattr(self, k) != v:
                return False
        return True


def parse_alog_line(line):
    print line
    rex = (
'^(?P<ip>\d+\.\d+\.\d+\.\d+)'
' (?P<clientid>\S+)'
' (?P<user>\S+)'
' \[(?P<datetime>.*)\]'
' \"(?P<verb>.*) /.*\"'
' (?P<resp_code>\d+)'
' (?P<resp_length>\d+)'
' (?P<referrer>.*)'
' \"(?P<useragent>.*)\"'
' (?P<req_length>\d+)')
    params = re.match(rex, line).groupdict()
    return LogEntry(**params)



if __name__ == "__main__":
    f = open('access.log')
    for line in f:
        parse_alog_line(line)
    f.close()


